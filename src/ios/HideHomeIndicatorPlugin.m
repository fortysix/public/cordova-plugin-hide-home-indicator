#import "HideHomeIndicatorPlugin.h"
#import <Cordova/CDVViewController.h>

@interface CDVViewController (HideHomeIndicator)
- (BOOL) prefersHomeIndicatorAutoHidden;
- (void) hide;
- (void) show;
@end

@implementation CDVViewController (HideHomeIndicator)
BOOL autoHide = NO;

- (BOOL) prefersHomeIndicatorAutoHidden {
    return autoHide;
}

- (void) hide
{
    autoHide = YES;
    [(CDVViewController *)self setNeedsUpdateOfHomeIndicatorAutoHidden];
}

- (void) show
{
    autoHide = NO;
    [(CDVViewController *)self setNeedsUpdateOfHomeIndicatorAutoHidden];
}
@end


@implementation HideHomeIndicatorPlugin

- (void) pluginInitialize
{
    NSLog(@"Plugin initialized, hiding home indicator by default");
}

- (void) hide:(CDVInvokedUrlCommand*)command
{
    [(CDVViewController *)self.viewController hide];
}

- (void) show:(CDVInvokedUrlCommand*)command
{
    [(CDVViewController *)self.viewController show];
}

@end
