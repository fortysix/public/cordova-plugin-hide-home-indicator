#import <Cordova/CDVPlugin.h>

@interface HideHomeIndicatorPlugin : CDVPlugin

- (void) hide:(CDVInvokedUrlCommand*)command;
- (void) show:(CDVInvokedUrlCommand*)command;

@end
