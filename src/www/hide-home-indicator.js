var exec = require("cordova/exec");

function HideHomeIndicator() {}

HideHomeIndicator.prototype.hide = function() {
  exec(function() {}, function() {}, "HideHomeIndicatorPlugin", "hide", []);
};
               
               
HideHomeIndicator.prototype.show = function() {
  exec(function() {}, function() {}, "HideHomeIndicatorPlugin", "show", []);
};

module.exports = new HideHomeIndicator();
